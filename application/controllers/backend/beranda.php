<?php
class beranda extends CI_Controller {
	function __construct() 
	{
		parent::__construct();
//            jika belum login redirect ke login
		if ($this->session->userdata('logged')<>1) {
			redirect(site_url('login'));
		}
		$this->load->model('m_pemetaan');
		$this->load->model('m_kawasan');
	}

	public function index()
	{

		$data['dtkabupaten']=$this->m_kawasan->get_all_kabupaten();
		$data['dtkecamatan']=$this->m_kawasan->get_kecamatan();

		$tahun=$this->input->get('2016');
		$kabupaten=$this->input->get('1');
		$this->db->order_by('tahun','DESC');
		$tahunterakhir=$this->db->get('tb_bayes')->result_array();
		
		$where=array(
			'tahun'=>$tahunterakhir[0]['tahun'],
		);
		$this->db->join('tb_desa','tb_bayes.id_desa=tb_desa.id_desa');
		$this->db->join('tb_kecamatan','tb_bayes.id_kecamatan=tb_kecamatan.id_kecamatan');
		$this->db->join('tb_kabupaten','tb_bayes.id_kabupaten=tb_kabupaten.id_kabupaten');
		$this->db->join('tb_klasifikasi','tb_bayes.id_bayes=tb_klasifikasi.id_bayes');

		$data['klasifikasi']=$this->db->get_where('tb_bayes',$where);

		$this->db->join('tb_desa','tb_bayes.id_desa=tb_desa.id_desa');

		$this->load->view('backend/v_beranda',$data);
	}	
}

