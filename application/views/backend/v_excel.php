<?php   header("Content-type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=Data Klasifikasi.xls"); ?>

            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table">
                <thead>
                <tr>
              			<th>No</th>
                    <th>Tahun</th>
              			<th>Kabupaten</th>
              			<th>Kecamatan</th>
              			<th>Desa</th>
              			<th>Tinggi Gelombang</th>
              			<th>Arus</th>
              			<th>Tipologi Pantai</th>
              			<th>Bentuk Garis Pantai</th>
              			<th>Tutupan Lahan</th>
                    <th>Klasifikasi</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  $no=0;
                  foreach ($klasifikasi->result_array() as $i) :
                     $no++;
                     $id_bayes=$i['id_bayes'];
                     $tahun=$i['tahun'];
                     $id_klasifikasi=$i['id_desa'];
                     $nm_kabupaten=$i['nm_kabupaten'];
                     $nm_kecamatan=$i['nm_kecamatan'];
                     $nm_desa=$i['nm_desa'];
                     $klasifikasi=$i['klasifikasi'];

                      $this->db->join('tb_subparameter','tb_subparameter.id_subparameter=tb_klasifikasi.id_subparameter');
                      $dtgelombang= $this->db->get_where('tb_klasifikasi',array('id_parameter'=>'0','id_bayes'=>$i['id_bayes']))->result_array();
                      $this->db->join('tb_subparameter','tb_subparameter.id_subparameter=tb_klasifikasi.id_subparameter');
                      $dtarus= $this->db->get_where('tb_klasifikasi',array('id_parameter'=>'1','id_bayes'=>$i['id_bayes']))->result_array();
                      $this->db->join('tb_subparameter','tb_subparameter.id_subparameter=tb_klasifikasi.id_subparameter');
                      $dttipologi= $this->db->get_where('tb_klasifikasi',array('id_parameter'=>'3','id_bayes'=>$i['id_bayes']))->result_array();
                      $this->db->join('tb_subparameter','tb_subparameter.id_subparameter=tb_klasifikasi.id_subparameter');
                      $dtgaris= $this->db->get_where('tb_klasifikasi',array('id_parameter'=>'2','id_bayes'=>$i['id_bayes']))->result_array();
                      $this->db->join('tb_subparameter','tb_subparameter.id_subparameter=tb_klasifikasi.id_subparameter');
                      $dtvegetasi= $this->db->get_where('tb_klasifikasi',array('id_parameter'=>'4','id_bayes'=>$i['id_bayes']))->result_array();
                      ?>
                
                  <tr>
                  <td><?php echo $no;?></td>
                  <td><?php echo $tahun;?></td>  
                  <td><?php echo $nm_kabupaten;?></td>
                  <td><?php echo $nm_kecamatan;?></td>
                  <td><?php echo $nm_desa;?></td>

                  <td><?php
                    echo $dtgelombang[0]['nm_subparameter']; 
                   ?></td>
                  
                  <td>
                   <?php 
                    echo $dtarus[0]['nm_subparameter'];
                  ?> 
                  </td>
                  
                  <td>
                    <?php 

                    echo $dttipologi[0]['nm_subparameter'];
                  ?>
                  </td>      
                  
                  <td>
                    <?php 
                    echo $dtgaris[0]['nm_subparameter'];
                  ?>
                  </td> 
                  
                  <td>
                    <?php 

                    echo $dtvegetasi[0]['nm_subparameter'];
                  ?>
                  </td>
                  <td>
                    <?php echo $klasifikasi; ?>
                  </td>
                  <td>
                    <a title="Ubah data" class="btn btn-primary" data-toggle="modal" data-target= "#ubahklasifikasi<?php echo $id_bayes?>"> <span class="fa fa-pencil"></span></a>
                    <a title="Hapus Data" class="btn btn-danger
                    " data-toggle="modal" data-target="#hapusklasifikasi<?php echo $id_bayes;?>"><span class="fa fa-trash"></span></a>
                  </td>
                </tr>
              <?php endforeach;?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          