

        <div class="modal fade" id="Modalkeluar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel"></h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'index.php/login/logout'?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                            <p>Apakah Anda yakin ingin <b>keluar</b> dari sistem ?</p>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Tidak</button>
                        <button type="submit" class="btn btn-primary btn-flat" >Keluar</button>


                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Modal Kabupaten: hapus Klasifiaksi -->


<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url().'assets/backend/plugins/jQuery/jquery-2.2.3.min.js'?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url().'assets/backend/bootstrap/js/bootstrap.min.js'?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url().'assets/backend/plugins/datatables/jquery.dataTables.min.js'?>"></script>
<script src="<?php echo base_url().'assets/backend/plugins/datatables/dataTables.bootstrap.min.js'?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url().'assets/backend/js/wow.min.js'?>"></script>
<script src="<?php echo base_url().'assets/backend/chart.js/Chart.js'?>"></script>

<script src="<?php echo base_url().'assets/backend/slimScroll/jquery.slimscroll.min.js'?>"></script>
<script src="<?php echo base_url().'assets/backend/plugins/datepicker/bootstrap-datepicker.js'?>"></script>
<script src="<?php echo base_url().'assets/backend/plugins/timepicker/bootstrap-timepicker.min.js'?>"></script>
<script src="<?php echo base_url().'assets/backend/plugins/daterangepicker/daterangepicker.js'?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url().'assets/backend/plugins/fastclick/fastclick.js'?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url().'assets/backend/dist/js/app.min.js'?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url().'assets/backend/dist/js/demo.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/backend/plugins/toast/jquery.toast.min.js'?>"></script>
<!-- page script -->

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });

    $("#datepicker").datepicker({
      format: "yyyy",
      autoclose: true,
      viewMode: "years",
      minViewMode: "years"
    });
    $('#datepicker2').datepicker({
      format: "yyyy",
      viewMode: "years",
      autoclose: true,
      minViewMode: "years"
    });
    $('.datepicker3').datepicker({
      format: "yyyy",
      autoclose: true,
      viewMode: "years",
      minViewMode: "years"
    });
    $('.datepicker4').datepicker({
      format: "yyyy",
      autoclose: true,
      viewMode: "years",
      minViewMode: "years"
    });
    $(".timepicker").timepicker({
      showInputs: true
    });

  });
</script>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

    <script>
        function nip(evt) {
          var charCode = (evt.which) ? evt.which : event.keyCode
           if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
          return true;
        }
        function huruf(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if ((charCode < 65 || charCode > 90)&&(charCode < 97 || charCode > 122)&&charCode>32)
            return false;
        return true;
}
    </script>

<script type="text/javascript">
function validasi(){
 var nama = document.getElementById("nama").value;
 if(nama == ""){
 alert("nama harus diisi");
 }else if (!cekuser(nama)) {
 alert("Isi Dengan Huruh");
 nama.focus();
 return false;
 }else{
 alert("Sudah Huruf Semua");
 }
}

</script>

     <?php if($this->session->flashdata('msg')=='hapusdata'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Sukses',
                    text: "Data Berhasil Dihapus",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#e6000b'
                });
        </script>
     <?php elseif($this->session->flashdata('msg')=='warning2'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Peringatan',
                    text: "Maaf Ukuran Foto Terlalu Besar.Foto Gagal ditambah",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#e6000b'
                });
        </script>
      <?php elseif($this->session->flashdata('msg')=='warning-nip'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Peringatan',
                    text: "Data Gagal Karna data Telah Ada.",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#e6000b'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='success'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Sukses',
                    text: "Data disimpan ke database.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>

    <?php elseif($this->session->flashdata('msg')=='info'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: "Data berhasil di update",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='success-hapus'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Sukses',
                    text: "Data  Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php else:?>

    <?php endif;?>
</body>
</html>

<script type="text/javascript">
$(function(){
  $.ajaxSetup({
  type:"POST",
  url: "<?php echo base_url('index.php/backend/pemetaan/ajax') ?>",
  cache: false,
});
  $("#tahun").change(function(){
    var value=$(this).val();
    if(value>0){
      $.ajax({
        data:{id:value},
        success: function(respond){
          $("#kabupaten").html(respond);
        }
      })
    }
  });
})
</script>


<script type="text/javascript">
  function initMap(){
    var map;
    var polygon=[];
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat:-3.635299, lng: 102.321318},
          zoom: 8
        });

        var data = <?php echo json_encode($data_kawasan)?>;
        var infowindow_kel=[];

        for(var x in data){
          var j = [];
          var kl = data[x][4];
          var ll = data[x][2].split("\n");


          for(var i=1;i<=ll.length;i++){
          var longlat = ll[i-1].split(",");
          j.push(new google.maps.LatLng(parseFloat(longlat[0]), parseFloat(longlat[1])));
      }


      var info = "<h3>"+data[x][1]+"</h3>";


      if(kl=='rendah'){
        color="green"
      }else if(kl=='sedang'){
        color="yellow"
      }else if(kl=='tinggi'){
        color="red"
      }

      var polygon_x = new google.maps.Polygon({
        paths: j,
          strokeColor: color,
          strokeOpacity: 0.8,
          strokeWeight: 3,
          fillColor: color,
          fillOpacity: 0.35
        });

      polygon.push(polygon_x);
      infowindow = new google.maps.InfoWindow({content: info,maxWidth:1000});
        infowindow_kel.push(infowindow);
        }

        for(var m=0; m<=polygon.length;m++){
      polygon[m].setMap(map);
      google.maps.event.addListener(polygon[m], 'click', (function(m) {
        infowindow_kel[m].close();
          return function(event){
            infowindow_kel[m].setPosition(event.latLng)
          infowindow_kel[m].open(map);
        }
      })(m));
    }
    }

  </script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkVqDh3ZeTTfLOxCm557neqs51wnzg-SM&callback=initMap"></script>
<?php
      $kluster1=$this->db->query('SELECT  tahun,klasifikasi,count(klasifikasi) as jumlah FROM `tb_bayes` WHERE klasifikasi="rendah" GROUP by tahun')->result_array();
      $kluster2=$this->db->query('SELECT  tahun,klasifikasi,count(klasifikasi) as jumlah FROM `tb_bayes` WHERE klasifikasi="sedang" GROUP by tahun')->result_array();
      $kluster3=$this->db->query('SELECT  tahun,klasifikasi,count(klasifikasi) as jumlah FROM `tb_bayes` WHERE klasifikasi="tinggi" GROUP by tahun')->result_array();
      $kluster[2016]['c1']=0;
      $kluster[2017]['c1']=0;
      $kluster[2018]['c1']=0;
      $kluster[2016]['c2']=0;
      $kluster[2017]['c2']=0;
      $kluster[2018]['c2']=0;
      $kluster[2016]['c3']=0;
      $kluster[2017]['c3']=0;
      $kluster[2018]['c3']=0;
      foreach($kluster1 as $c1){
        if($c1['tahun']==2016){
          $kluster[2016]['c1']=$c1['jumlah'];
        }
        if($c1['tahun']==2017){
          $kluster[2017]['c1']=$c1['jumlah'];
        }
        if($c1['tahun']==2018){
          $kluster[2018]['c1']=$c1['jumlah'];
        }
      }

      foreach($kluster2 as $c2){
        if($c2['tahun']==2016){
          $kluster[2016]['c2']=$c2['jumlah'];
        }
        if($c2['tahun']==2017){
          $kluster[2017]['c2']=$c2['jumlah'];
        }
        if($c2['tahun']==2018){
          $kluster[2018]['c2']=$c2['jumlah'];
        }
      }

      foreach($kluster3 as $c3){
        if($c3['tahun']==2016){
          $kluster[2016]['c3']=$c3['jumlah'];
        }
        if($c3['tahun']==2017){
          $kluster[2017]['c3']=$c3['jumlah'];
        }
        if($c3['tahun']==2018){
          $kluster[2018]['c3']=$c3['jumlah'];
        }
      }



?>
<script type="text/javascript">
  $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $('#areaChart').get(0).getContext('2d')
    // This will get the first returned node in the jQuery collection.
    var areaChart       = new Chart(areaChartCanvas)

    var areaChartData = {
      labels  : ['2016', '2017', '2018'],
      datasets: [
        {
          label               : 'Digital Goods',
          fillColor           : 'rgba(60,141,188,0.9)',
          strokeColor         : 'rgba(60,141,188,0.8)',
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [<?php echo $kluster[2016]['c1'].",".$kluster[2017]['c1'].",".$kluster[2018]['c1']."," ?> ]
        },
        {
          label               : 'Digital Goods',
          fillColor           : 'rgba(60,141,188,0.9)',
          strokeColor         : 'rgba(60,141,188,0.8)',
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [<?php echo $kluster[2016]['c2'].",".$kluster[2017]['c2'].",".$kluster[2018]['c2']."," ?>]
        },
        {
          label               : '2018',
          fillColor           : 'rgba(1,141,188,0.9)',
          strokeColor         : 'rgba(60,141,188,0.8)',
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [<?php echo $kluster[2016]['c3'].",".$kluster[2017]['c3'].",".$kluster[2018]['c3']."," ?>]
        },
      ]
    }

    var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale               : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : false,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - Whether the line is curved between points
      bezierCurve             : true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension      : 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot                : false,
      //Number - Radius of each point dot in pixels
      pointDotRadius          : 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth     : 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius : 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke           : true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth      : 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill             : true,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio     : true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive              : true
    }

    //Create the line chart
    areaChart.Line(areaChartData, areaChartOptions)

    //-------------
    //- LINE CHART -
    //--------------
    var lineChartCanvas          = $('#lineChart').get(0).getContext('2d')
    var lineChart                = new Chart(lineChartCanvas)
    var lineChartOptions         = areaChartOptions
    lineChartOptions.datasetFill = false
    lineChart.Line(areaChartData, lineChartOptions)

    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
    var pieChart       = new Chart(pieChartCanvas)
    var PieData        = [
      {
        value    : 700,
        color    : '#f56954',
        highlight: '#f56954',
        label    : 'Chrome'
      },
      {
        value    : 500,
        color    : '#00a65a',
        highlight: '#00a65a',
        label    : 'IE'
      },
      {
        value    : 400,
        color    : '#f39c12',
        highlight: '#f39c12',
        label    : 'FireFox'
      },
      {
        value    : 600,
        color    : '#00c0ef',
        highlight: '#00c0ef',
        label    : 'Safari'
      },
      {
        value    : 300,
        color    : '#3c8dbc',
        highlight: '#3c8dbc',
        label    : 'Opera'
      },
      {
        value    : 100,
        color    : '#d2d6de',
        highlight: '#d2d6de',
        label    : 'Navigator'
      }
    ]
    var pieOptions     = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke    : true,
      //String - The colour of each segment stroke
      segmentStrokeColor   : '#fff',
      //Number - The width of each segment stroke
      segmentStrokeWidth   : 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps       : 100,
      //String - Animation easing effect
      animationEasing      : 'easeOutBounce',
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate        : true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale         : false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive           : true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio  : true,
      //String - A legend template
      legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions)

    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas                   = $('#barChart').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = areaChartData
    barChartData.datasets[0].fillColor   = '#00ff00'
    barChartData.datasets[0].strokeColor = '#00ff00'
    barChartData.datasets[0].pointColor  = '#00ff00'
    barChartData.datasets[1].fillColor   = '#ffff00'
    barChartData.datasets[1].strokeColor = '#ffff00'
    barChartData.datasets[1].pointColor  = '#ffff00'
    barChartData.datasets[2].fillColor   = '#ff0000'
    barChartData.datasets[2].strokeColor = '#ff0000'
    barChartData.datasets[2].pointColor  = '#ff0000'
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions)
  })
</script>

        <script src="<?php echo base_url('assets/js/jquery.chained.min.js') ?>"></script>

   <script>
          $("#xid_kecamatan").chained("#xid_kabupaten");
          $("#xid_desa").chained("#xid_kecamatan"); // disini kita hubungkan kecamatan dengan kota
    </script>

