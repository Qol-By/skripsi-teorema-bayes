<?php
class pemetaan extends CI_Controller {
	function __construct() 
	{
		parent::__construct();
//            jika belum login redirect ke login
		if ($this->session->userdata('logged')<>1) {
			redirect(site_url('login'));
		}

		$this->load->model('m_kawasan');
	}

	public function ajax(){
		$this->load->model('m_pemetaan');
		echo $this->m_pemetaan->ajax();
	}


	public function index()
	{
		$data['dtkabupaten']=$this->m_kawasan->get_all_kabupaten();
		$data['dtkecamatan']=$this->m_kawasan->get_kecamatan();

		$tahun=$this->input->get('tahun');
		$kabupaten=$this->input->get('kabupaten');
		
		$where=array(
			'tahun'=>$tahun,
			'tb_bayes.id_kabupaten'=>$kabupaten
		);

		$this->db->join('tb_desa','tb_bayes.id_desa=tb_desa.id_desa');
		$this->db->join('tb_kecamatan','tb_bayes.id_kecamatan=tb_kecamatan.id_kecamatan');
		$this->db->join('tb_kabupaten','tb_bayes.id_kabupaten=tb_kabupaten.id_kabupaten');
		

		$data['klasifikasi']=$this->db->get_where('tb_bayes',$where);

		$this->db->join('tb_desa','tb_bayes.id_desa=tb_desa.id_desa');

		$this->load->view('backend/v_pemetaan',$data);
	}

	public function index2(){
		$tahun=$this->input->get('tahun');
		$kabupaten=$this->input->get('kabupaten');
		$where=array(
			'tahun'=>$tahun,
			'tb_bayes.id_kabupaten'=>$kabupaten
		);

		$this->db->join('tb_desa','tb_bayes.id_desa=tb_desa.id_desa');
		$data[]=$this->db->get_where('tb_bayes',$where)->result_array();
		
		redirect('backend/pemetaan');
	}	
}

