
<!-- Modal Kabupaten: tambah Desa -->
      <div class="modal fade" id="tambahkawasan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel">Data Kawasan</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url().'index.php/backend/kawasan/simpan_desa'?>" method="post" enctype="multipart/form-data">
              <div class="modal-body">
              <div class="form-group">
                  <label class="col-sm-4 control-label">Kabupaten</label>
                  <div class="col-sm-7">
                    <select name="xid_kabupaten" id="xid_kabupaten" class="form-control select2" required>
                        <option value="">Pilih Kabupaten</option>
                            <?php
                            foreach ($kabupaten->result() as $kab) {
                                ?>
                                <option <?php echo $kabupaten_selected == $kab->id_kabupaten ? 'selected="selected"' : '' ?> 
                                    value="<?php echo $kab->id_kabupaten ?>"><?php echo $kab->nm_kabupaten ?></option>
                                <?php
                            }
                            ?>
                      </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-4 control-label">Kecamatan </label>
                  <div class="col-sm-7">
                  <select class="form-control" name="xid_kecamatan" id="xid_kecamatan">
                    <option value="">Pilih Kecamatan</option>
                      <?php
                      foreach ($dtkecamatan as $kec) {
                          ?>
                          <option <?php echo $kecamatan_selected == $kec->id_kecamatan ? 'selected="selected"' : '' ?> 
                              class="<?php echo $kec->id_kabupaten ?>" value="<?php echo $kec->id_kecamatan ?>"><?php echo $kec->nm_kecamatan ?></option>
                          <?php
                      }
                      ?>
                  </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Nama Desa</label>
                  <div class="col-sm-7">
                    <input type="text" name="xnm_desa" class="form-control" id="inputUserName" placeholder="Nama Desa" required>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Polygon Desa</label>
                  <div class="col-sm-7">
                       <textarea type="textarea" name="xpolygon_desa" class="form-control" required></textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">marker Desa</label>
                  <div class="col-sm-7">
                    <input type="text" name="xmarker_desa" class="form-control" id="inputUserName" placeholder="marker Desa" required>
                  </div>
                </div>
                                                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div> 
<!-- Modal Kabupaten: hapus Kecamatan -->
     <?php
      $no=0;
      foreach ($desa->result_array() as $i) :
         $no++;
         $id_desa=$i['id_desa'];
         $nm_desa=$i['nm_desa'];
      ?> 

      <div class="modal fade" id="hapusdesa<?php echo $id_desa;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel">Data Kecamatan</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url().'index.php/backend/kawasan/hapus_desa'?>" method="post" enctype="multipart/form-data">
              <div class="modal-body">
                <div class="form-group">
                  <div class="col-sm-7">
                     <input value='<?php echo $id_desa;?>' type="hidden" name="xid_desa"> 
                        <p>Apakah Anda yakin mau menghapus Data <b><?php echo $nm_desa;?></b> ?</p>
                  </div>
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger btn-flat" id="simpan">Hapus</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <?php endforeach;?>



<!---------------------Modal Kabupaten--------------------->
 <div class="modal fade" id="modalkabupaten" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
            <h4 class="modal-title" id="myModalLabel">Data Kabupaten</h4>
          </div>
          <section class="content">
            <div class="row">                  
              <div class="box">
                <div class="box-header">
<!--                  <a class="btn btn-success btn-flat" data-toggle="modal" data-target="#addkabupaten"><span class="fa fa-plus"></span>Tambah Data</a>-->
                  </div>

                  <div class="box-body">
                    <table id="example1" class="table table">
                      
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Kabupaten</th>
                        </tr>
                      </thead>
                      
                      <tbody>
                      <?php
                        $no=0;
                        foreach ($kabupaten->result_array() as $i) :
                           $no++;
                           $id_kabupaten=$i['id_kabupaten'];
                           $nm_kabupaten=$i['nm_kabupaten'];

                        ?>                        
                        <tr>
                          <td><?php echo $no;?></td> 
                          <td><?php echo $nm_kabupaten;?></td>
                          <!-- <td>
                            <a title="Ubah Kabupaten" class="btn btn-primary" data-toggle="modal" data-target="#ubahkabupaten<?php echo $id_kabupaten;?>"><span class="fa fa-pencil"></span></a>
                            <a title="Hapus Kabupaten" class="btn btn-danger" data-toggle="modal" data-target="#hapuskabupaten<?php echo $id_kabupaten;?>"><span class="fa fa-trash"></span></a>
                          </td>-->
                        </tr>
                        <?php endforeach;?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>  
            </section>
          </div>
        </div>
      </div>  

<!-- Modal Kabupaten: tambah kabupaten -->
      <div class="modal fade" id="addkabupaten" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel">Data Kabupaten</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url().'index.php/backend/kawasan/simpan_kabupaten'?>" method="post" enctype="multipart/form-data">
              <div class="modal-body">
                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Nama Kabupaten</label>
                  <div class="col-sm-7">
                    <input type="text" name="xnm_kabupaten" class="form-control" id="inputUserName" placeholder="Nama Kabupaten" required>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div> 

<!-- Modal Kabupaten: Ubah Kabupaten -->
    <?php
      $no=0;
      foreach ($kabupaten->result_array() as $i) :
         $no++;
         $id_kabupaten=$i['id_kabupaten'];
         $nm_kabupaten=$i['nm_kabupaten'];

      ?> 
      <div class="modal fade" id="ubahkabupaten<?php echo $id_kabupaten;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel">Ubah Data Kabupaten</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url().'index.php/backend/kawasan/ubah_kabupaten'?>" method="post" enctype="multipart/form-data">
              <div class="modal-body">
                <div class="form-group">
                  <input type="hidden" name="xid_kabupaten" value="<?php echo $id_kabupaten;?>"/>
                  <label for="inputUserName" class="col-sm-4 control-label">Nama Kabupaten</label>
                  <div class="col-sm-7">
                    <input type="text" name="xnm_kabupaten" class="form-control" id="inputUserName" placeholder="Nama Kabupaten" value="<?php echo $nm_kabupaten;?>" required>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div> 
      <?php endforeach;?>

<!-- Modal Kabupaten: hapus kabupaten -->
    <?php
      $no=0;
      foreach ($kabupaten->result_array() as $i) :
         $no++;
         $id_kabupaten=$i['id_kabupaten'];
         $nm_kabupaten=$i['nm_kabupaten'];

      ?> 
      <div class="modal fade" id="hapuskabupaten<?php echo $id_kabupaten;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel">Data Kabupaten</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url().'index.php/backend/kawasan/hapus_kabupaten'?>" method="post" enctype="multipart/form-data">
              <div class="modal-body">
                <div class="form-group">
                  <div class="col-sm-7">
                     <input value='<?php echo $id_kabupaten;?>' type="hidden" name="xid_kabupaten"> 
                        <p>Apakah Anda yakin mau menghapus Data <b><?php echo $nm_kabupaten;?></b> ?</p>
                  </div>
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger btn-flat" id="simpan">Hapus</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <?php endforeach;?>       

<!---------------------Modal Kecamatan--------------------->
 <div class="modal fade" id="modalkecamatan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
            <h4 class="modal-title" id="myModalLabel">Data Kabupaten</h4>
          </div>
          <section class="content">
            <div class="row">                  
              <div class="box">
                <div class="box-header">
<!--                  <a class="btn btn-success btn-flat" data-toggle="modal" data-target="#addkecamatan"><span class="fa fa-plus"></span>Tambah Data</a>-->
                  </div>

                  <div class="box-body">
                    <table id="example1" class="table table">
                      
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Kabupaten</th>
                          <th>Kecamatan</th>
                        </tr>
                      </thead>
                      
                      <tbody>
                      <?php
                        $no=0;
                        foreach ($kecamatan->result_array() as $i) :
                           $no++;
                           $id_kecamatan=$i['id_kecamatan'];
                           $nm_kabupaten=$i['nm_kabupaten'];
                           $nm_kecamatan=$i['nm_kecamatan'];
                        ?>                        
                        <tr>
                          <td><?php echo $no;?></td> 
                          <td><?php echo $nm_kabupaten;?></td>
                          <td><?php echo $nm_kecamatan;?></td>                          
                          <!-- <td>
                            <a title="Ubah Kecamatan" class="btn btn-primary" data-toggle="modal" data-target="#ubahkecamatan<?php echo $id_kecamatan;?>"><span class="fa fa-pencil"></span></a>
                            <a title="Hapus Kecamatan" class="btn btn-danger" data-toggle="modal" data-target="#hapuskecamatan<?php echo $id_kecamatan;?>"><span class="fa fa-trash"></span></a>
                          </td>-->
                        </tr>
                        <?php endforeach;?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>  
            </section>
          </div>
        </div>
      </div>  

<!-- Modal Kabupaten: tambah Kecamatan -->
      <div class="modal fade" id="addkecamatan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel">Data Kecamatan</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url().'index.php/backend/kawasan/simpan_kecamatan'?>" method="post" enctype="multipart/form-data">
              <div class="modal-body">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Kabupaten</label>
                  <div class="col-sm-7">
                    <select name="xid_kabupaten" class="form-control select2" required>
                      <option value="">-Pilih-</option>
                        <?php
                          $no=0;
                          foreach ($kabupaten->result_array() as $i) :
                            $no++;
                                $id_kabupaten=$i['id_kabupaten'];
                                $nm_kabupaten=$i['nm_kabupaten'];           
                            ?>
                      <option value="<?php echo $id_kabupaten;?>"><?php echo $nm_kabupaten;?></option>
                        <?php endforeach;?>
                    </select>
                  </div>
                </div>
               <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Nama Kecamatan</label>
                  <div class="col-sm-7">
                    <input type="text" name="xnm_kecamatan" class="form-control" id="inputUserName" placeholder="Nama Kabupaten" required>
                  </div>
                </div>                                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div> 

<!-- Modal Kabupaten: Ubah Kabupaten -->
     <?php
      $no=0;
      foreach ($kecamatan->result_array() as $i) :
         $no++;
         $id_kecamatan=$i['id_kecamatan'];
         $id_kabupaten=$i['id_kabupaten'];
         $nm_kecamatan=$i['nm_kecamatan'];

      ?>
      <div class="modal fade" id="ubahkecamatan<?php echo $id_kecamatan;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel">Ubah Data Kabupaten</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url().'index.php/backend/kawasan/ubah_kecamatan'?>" method="post" enctype="multipart/form-data">
              <div class="modal-body">
              <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Desa</label>
                  <input type="hidden" name="xid_kecamatan" value="<?php echo $id_kecamatan;?>"/>
                  <div class="col-sm-7">  
                      <select class="form-control select2" name="xid_kabupaten" required>
                          <option value="">-Pilih-</option>
                            <?php
                              $no=0;
                              foreach ($kabupaten->result_array() as $i) :
                                $no++;
                                    $id_kabupaten=$i['id_kabupaten'];
                                    $nm_kabupaten=$i['nm_kabupaten'];           
                                ?>
                          <option  
                          <?php if($id_kabupaten==$id_kabupaten) {echo "selected";} ?> value="<?php echo $id_kabupaten;?>"><?php echo $nm_kabupaten;?> </option>
                            <?php 
                                endforeach;
                            ?>
                      </select>
                    </div>
              </div>
                      
              <div class="modal-body">
                <div class="form-group">                
                    <label for="inputUserName" class="col-sm-4 control-label">Nama Kabupaten</label>
                  <div class="col-sm-7">
                    <input type="text" name="xnm_kecamatan" class="form-control" id="inputUserName" placeholder="Nama Kabupaten" value="<?php echo $nm_kecamatan;?>" required>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div> 
      <?php endforeach;?>

<!-- Modal Kabupaten: hapus Kecamatan -->
     <?php
      $no=0;
      foreach ($kecamatan->result_array() as $i) :
         $no++;
         $id_kecamatan=$i['id_kecamatan'];
         $nm_kecamatan=$i['nm_kecamatan'];
      ?> 

      <div class="modal fade" id="hapuskecamatan<?php echo $id_kecamatan;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel">Data Kecamatan</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url().'index.php/backend/kawasan/hapus_kecamatan'?>" method="post" enctype="multipart/form-data">
              <div class="modal-body">
                <div class="form-group">
                  <div class="col-sm-7">
                     <input value='<?php echo $id_kecamatan;?>' type="hidden" name="xid_kecamatan"> 
                        <p>Apakah Anda yakin mau menghapus Data <b><?php echo $nm_kecamatan;?></b> ?</p>
                  </div>
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger btn-flat" id="simpan">Hapus</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <?php endforeach;?>