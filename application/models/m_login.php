<?php
defined('BASEPATH') OR exit('No direct script access allowed');
  class m_login extends CI_Model {
 
//    untuk mengcek jumlah username dan password yang sesuai
function login($username,$password) {
  $this->db->where('nm_user', $username);
  $this->db->where('pwd_user', $password);
  $query =  $this->db->get('tb_user');
  return $query->num_rows();
}

//    untuk mengambil data hasil login
function data_login($username,$password) {
  $this->db->where('nm_user', $username);
  $this->db->where('pwd_user', $password);
  return $this->db->get('tb_user')->row();
}
        }