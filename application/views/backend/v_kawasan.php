  <?php 
    $this->load->view('backend/v_header');
  ?>
  <aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
        <li class="header">Menu Utama</li>
        <li>
          <a href="<?php echo base_url().'index.php/backend/beranda'?>">
            <i class="fa fa-home"></i> <span>Beranda</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <li class="treeview active">
          <a>
            <i class="fa fa-archive"></i>
            <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="<?php echo base_url().'index.php/backend/kawasan'?>"> <i class="fa fa-list-alt"></i>Kawasan</li>
            <li><a href="<?php echo base_url().'index.php/backend/parameter'?>"><i class="fa fa-list"></i>Parameter</a></li>
            <li><a href="<?php echo base_url().'index.php/backend/klasifikasi'?>"><i class="fa fa-wrench"></i>Klasifikasi Laju Abrasi Pantai</a></li>
          </ul>
        </li>
         <li><a href="<?php echo base_url().'index.php/backend/pemetaan'?>"> <i class="fa fa-map-o"></i> <span>Peta Kawasan Abrasi</span><span class="pull-right-container"><small class="label pull-right"></small></span></a></li>
         <li>
          <a data-target="#Modalkeluar" data-toggle="modal">
            <i class="fa fa-sign-out"></i> <span>Keluar</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Kawasan
        <small></small>
      </h1>
    </section>
		

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
           		<a class="menu-ico">
           			<div class="col-lg-2 col-md-3 text-center services border-right">
           				<div class="service-box">
           					<h4><b>KABUPATEN</b></h4>
           					<div class="ico primary">
           						  <i class="fa fa-3x fa fa-university wow bounceIn"></i>
           					</div>
                    <br>
           					<button data-target="#modalkabupaten" data-toggle="modal"  class="btn btn-primary">Lihat Data</button>
           				</div>
           			</div>
           		</a>

           		<a href="#" class="menu-ico">
           			<div class="col-lg-2 col-md-3 text-center services border-right">
           				<div class="service-box">
           					<h4><b>KECAMATAN</b></h4>
           					<div class="ico primary">
           						  <i class="fa fa-3x fa fa-bank wow bounceIn" data-wow-delay=".1s"></i>
           					</div>
                    <br>
                    <button data-target="#modalkecamatan" data-toggle="modal"  class="btn btn-primary">Lihat Data</button></div>
           			</div>
           		</a>

     
            </div>
            <div class="box-header">
              <button data-target="#tambahkawasan" data-toggle="modal" class="btn btn-success">Tambah Kawasan</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table">
                <thead>
                <tr>
					        <th>No</th>
					        <th>Kabupaten</th>
                  <th>Kecamatan</th>
                  <th>Desa</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>

                <?php
                  $no=0;
                  foreach ($kawasan->result_array() as $i) :
                     $no++;
                     $id_desa=$i['id_desa'];
                     $id_kecamatan=$i['id_kecamatan'];
                     $nm_kabupaten=$i['nm_kabupaten'];
                     $nm_kecamatan=$i['nm_kecamatan'];
                     $nm_desa=$i['nm_desa'];
                   
                  ?>                  
                <tr>
                  <td><?php echo $no;?></td>	
                  <td><?php echo $nm_kabupaten;?></td>
                  <td><?php echo $nm_kecamatan;?></td>
                  <td><?php echo $nm_desa;?></td>
                  
                  <td>

                    <a title="Ubah data" class="btn btn-primary" href="<?php echo base_url().'index.php/backend/kawasan/ubah/'.$id_desa;?>"><span class="fa fa-pencil"></span></a>
                    <a title="Hapus Data" class="btn btn-danger
                    " data-toggle="modal" data-target="#hapusdesa<?php echo $id_desa;?>"><span class="fa fa-trash"></span></a>
                  </td>                  
                </tr>
                <?php endforeach;?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2019.</strong> All rights reserved.
  </footer>
<!---------------------Modal Desa--------------------->


  <?php 
    $this->load->view('backend/v_kawasan_modal');
  ?>

  <?php 
    $this->load->view('backend/v_footer');
  ?>
