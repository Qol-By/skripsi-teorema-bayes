<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>SISTEM BERBERBASIS PENGETAHUAN PENENTUAN DAERAH RAWAN BENCANA LAJU ABRASI PANTAI BENGKULU TENGAH</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="<?php echo base_url().'assets/frontend/img/favicon.png'?>" rel="icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Roboto:100,300,400,500,700|Philosopher:400,400i,700,700i" rel="stylesheet">

  <!-- Bootstrap css -->
  <!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
  <link href="<?php echo base_url().'assets/frontend/lib/bootstrap/css/bootstrap.min.css'?>" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="<?php echo base_url().'assets/frontend/lib/owlcarousel/assets/owl.carousel.min.css'?>" rel="stylesheet">
  <link href="<?php echo base_url().'assets/frontend/lib/owlcarousel/assets/owl.theme.default.min.css'?>" rel="stylesheet">
  <link href="<?php echo base_url().'assets/frontend/lib/font-awesome/css/font-awesome.min.css'?>" rel="stylesheet">
  <link href="<?php echo base_url().'assets/frontend/lib/animate/animate.min.css'?>" rel="stylesheet">
  <link href="<?php echo base_url().'assets/frontend/lib/modal-video/css/modal-video.min.css'?>" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="<?php echo base_url().'assets/frontend/css/style.css'?>" rel="stylesheet">

</head>

<body>

  <header id="header" class="header header-hide">
    <div class="container">

      <div id="logo" class="pull-left">
        <h1><a href="#body" class="scrollto"><img src="<?php echo base_url().'assets/frontend/img/sbp2.png'?>" alt="" title="" /></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#body"><img src="img/logo.png" alt="" title="" /></a>-->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="#hero">Beranda</a></li>
          <li><a href="#get-started">Metode</a></li>
          <li><a href="#cek_maps">Lokasi Tingkat Rawan Bencana</a></li>
          <li><a href="<?php echo base_url().'index.php/login'?>">Login</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->

  <!--==========================
    Hero Section
  ============================-->
  <section id="hero" class="wow fadeIn">
    <div class="hero-container">
      <h1>SISTEM INFORMASI ABRASI</h1>
      <img src="<?php echo base_url().'assets/frontend/img/hero-img.png'?>" alt="Hero Imgs">
    </div>
  </section><!-- #hero -->

  <!--==========================
    Get Started Section
  ============================-->
  <section id="get-started" class="padd-section text-center wow fadeInUp">

    <div class="container">
      <div class="section-title text-center">
        <h2>Metode Teorema Bayes </h2>
      </div>
    </div>

    <div class="container">
      <div class="row">

        <div class="col-md-6 col-lg-4">
          <div class="feature-block">

            <img src="<?php echo base_url().'assets/frontend/img/svg/cloud.svg'?>" alt="img" class="img-fluid">
            <h4>Tentang Metode</h4>
            <p>Teorema Bayes adalah jenis metode yang terdapat pada Sistem Pakar telah banyak digunakan untuk menemukan solusi permasalahan yang berkaitan tentang probabilitas. Teorema Bayes adalah metode yang menerapkan aturan yang dihubungkan dengan nilai probabilitas atau kemungkinan untuk menghasilkan suatu keputuasan dan informasi yang tepat berdasarkan penyebab-penyebab terjadinya</p>
          </div>
        </div>

        <div class="col-md-6 col-lg-4">
          <div class="feature-block">

            <img src="<?php echo base_url().'assets/frontend/img/svg/planet.svg'?>" alt="img" class="img-fluid">
            <h4>Abrasi Pantai</h4>
            <p>Abrasi merupakan pengikisan atau pengurangan daratan (pantai) akibat aktivitas gelombang, arus dan pasang surut. 
              Dalam kaitan ini pemadatan daratan mengakibatkan permukaan tanah turun dan tergenang air laut sehingga garis pantai berubah</p>
          </div>
        </div>

        <div class="col-md-6 col-lg-4">
          <div class="feature-block">

            <img src="<?php echo base_url().'assets/frontend/img/svg/asteroid.svg'?>" alt="img" class="img-fluid">
            <h4>Klasifikasi Rawan Bencana Abrasi Pantai</h4>
            <p>Rawan bencana abrasi pantai terdiri dari tiga klasifikasi yaitu: aman, sedang dan tinggi</p>

          </div>
        </div>

      </div>
    </div>

  </section>

  <!--==========================
    Contact Section
  ============================-->
  <section id="cek_maps" class="padd-section wow fadeInUp">

    <div class="container">
      <div class="section-title text-center">
        <h2>Lokasi Tingkat Rawan Bencana Laju Abrasi Pesisir Pantai Bengkulu Tengah Dan Bengkulu Utara</h2>
      </div>
    </div>

    <div class="container">
      <div class="row justify-content-center">
      <div class="col-md-8 col-lg-10 ">
            <div class="container-fluid">
              <div class="box-body">
                <div id="map" style="width:100%;height:800px;"></div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </section><!-- #contact -->

  <!--==========================
    Footer
  ============================-->
  <footer class="footer">
  </footer>



  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="<?php echo base_url().'assets/frontend/lib/jquery/jquery.min.js'?>"></script>
  <script src="<?php echo base_url().'assets/frontend/lib/jquery/jquery-migrate.min.js'?>"></script>
  <script src="<?php echo base_url().'assets/frontend/lib/bootstrap/js/bootstrap.bundle.min.js'?>"></script>
  <script src="<?php echo base_url().'assets/frontend/lib/superfish/hoverIntent.js'?>"></script>
  <script src="<?php echo base_url().'assets/frontend/lib/superfish/superfish.min.js'?>"></script>
  <script src="<?php echo base_url().'assets/frontend/lib/easing/easing.min.js'?>"></script>
  <script src="<?php echo base_url().'assets/frontend/lib/modal-video/js/modal-video.js'?>"></script>
  <script src="<?php echo base_url().'assets/frontend/lib/owlcarousel/owl.carousel.min.js'?>"></script>
  <script src="<?php echo base_url().'assets/frontend/lib/wow/wow.min.js'?>"></script>
  <!-- Contact Form JavaScript File -->
  <script src="<?php echo base_url().'assets/frontend/contactform/contactform.js'?>"></script>

  <!-- Template Main Javascript File -->
  <script src="<?php echo base_url().'assets/frontend/js/main.js'?>"></script>
<?php 
  $data_kawasan=array();
  foreach($klasifikasi->result() as $p){
    $data_kawasan[$p->id_bayes][0]=$p->id_desa;
    $data_kawasan[$p->id_bayes][1]=$p->nm_desa;
    $data_kawasan[$p->id_bayes][2]=$p->polygon_desa;
    $data_kawasan[$p->id_bayes][4]=$p->klasifikasi;
    $data_kawasan[$p->id_bayes][5]=$p->tahun;
    $data_kawasan[$p->id_bayes][6]=$p->id_kecamatan;
    $data_kawasan[$p->id_bayes][7]=$p->nm_kecamatan;
    $data_kawasan[$p->id_bayes][8]=$p->id_kabupaten;
    $data_kawasan[$p->id_bayes][9]=$p->nm_kabupaten;

?>
<?php } ?>        
<script type="text/javascript">
  function initMap(){
    var map;
    var polygon=[]; 
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat:-3.635299, lng: 102.321318}, 
          zoom: 8
        });
       
        var data = <?php echo json_encode($data_kawasan)?>;
        var infowindow_kel=[];

        for(var x in data){
          var j = [];
          var kl = data[x][4];
          var ll = data[x][2].split("\n");


          for(var i=1;i<=ll.length;i++){
          var longlat = ll[i-1].split(",");
          j.push(new google.maps.LatLng(parseFloat(longlat[0]), parseFloat(longlat[1]))); 
      }
      
      
      var info = "<h3>"+data[x][1]+"</h3>";


      if(kl=='rendah'){
        color="green"
      }else if(kl=='sedang'){
        color="yellow"                
      }else if(kl=='tinggi'){
        color="red"               
      }

      var polygon_x = new google.maps.Polygon({
        paths: j,
          strokeColor: color,
          strokeOpacity: 0.8,
          strokeWeight: 3,
          fillColor: color,
          fillOpacity: 0.35
        });

      polygon.push(polygon_x);
      infowindow = new google.maps.InfoWindow({content: info,maxWidth:1000});
        infowindow_kel.push(infowindow);
        }

        for(var m=0; m<=polygon.length;m++){
      polygon[m].setMap(map);
      google.maps.event.addListener(polygon[m], 'click', (function(m) {
        infowindow_kel[m].close();
          return function(event){
            infowindow_kel[m].setPosition(event.latLng) 
          infowindow_kel[m].open(map);
        }
      })(m)); 
    }  
  }

  </script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkVqDh3ZeTTfLOxCm557neqs51wnzg-SM&callback=initMap">
 </script>
</body>
</html>
