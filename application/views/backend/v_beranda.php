<?php 
    $this->load->view('backend/v_header');
  ?>
  <aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
        <li class="header">Menu Utama</li>
        <li class="active">
          <a href="<?php echo base_url().'index.php/backend/beranda'?>">
            <i class="fa fa-home"></i> <span>Beranda</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-archive"></i>
            <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url().'index.php/backend/kawasan'?>"> <i class="fa fa-list-alt"></i>Kawasan</li>
            <li><a href="<?php echo base_url().'index.php/backend/parameter'?>"><i class="fa fa-list"></i>Parameter</a></li>
            <li><a href="<?php echo base_url().'index.php/backend/klasifikasi'?>"><i class="fa fa-wrench"></i>Klasifikasi Laju Abrasi Pantai</a></li>
          </ul>
        </li>
         <li><a href="<?php echo base_url().'index.php/backend/pemetaan'?>"> <i class="fa fa-map-o"></i> <span>Peta Kawasan Abrasi</span><span class="pull-right-container"><small class="label pull-right"></small></span></a></li>
         <li>
          <a data-target="#Modalkeluar" data-toggle="modal">
            <i class="fa fa-sign-out"></i> <span>Keluar</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.s -->
      </aside>

 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pemetaan Tingkat Rawan Bencana Laju Abrasi Pantai Bengkulu Tengah dan Bengkulu Utara
        <small></small>
      </h1>

  <!-- Main content -->
    <section class="content">
      <div class="row">

        <div class="col-md-8">
          <div class="box box-primary" >
            <div class="container-fluid">
              <div class="box-body">
                <div id="map" style="width:100%;height:400px;"></div>
              </div>
      <h4>Keterangan Warna Klasifikasi Tingkat Rawan Bencana Laju Abrasi Pantai :</h4><br>
      <div class="col-md-8">
      <span class="btn btn-success"> Aman </span> 
      <span class="btn btn-warning"> Sedang </span>
      <span class="btn btn-danger"> Tinggi</span>
      </div>
      <br>


      <div class="col-md-4">
 
      </div>
      <br>
            </div>
          </div>
          <!-- AREA CHART -->
          <div class="box box-primary" style="display: none;">
            <div class="box-header with-border">
              <h3 class="box-title">Area Chart</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="areaChart" style="height:250px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- DONUT CHART -->
          <div class="box box-danger" style="display: none;">
            <div class="box-header with-border">
              <h3 class="box-title">Donut Chart</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <canvas id="pieChart" style="height:250px"></canvas>
            </div>

            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col (LEFT) -->
        <div class="col-md-4">
          <!-- LINE CHART -->
          <div class="box box-info" style="display: none;">
            <div class="box-header with-border">
              <h3 class="box-title">Line Chart</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="lineChart" style="height:250px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- BAR CHART -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Grafik Tingkat Rawan Bencana Pertahun</h3>


            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="barChart" style="height:230px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col (RIGHT) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  
    </section>

    <!-- Main content -->

    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">

        </div>
      </div>
    </section>
  </div>

    <!-- /.content -->

    <?php 
    $this->load->view('backend/v_footer');
  ?>

<?php 
  $data_kawasan=array();
  foreach($klasifikasi->result() as $p){
    $data_kawasan[$p->id_bayes][0]=$p->id_desa;
    $data_kawasan[$p->id_bayes][1]=$p->nm_desa;
    $data_kawasan[$p->id_bayes][2]=$p->polygon_desa;
    $data_kawasan[$p->id_bayes][4]=$p->klasifikasi;
    $data_kawasan[$p->id_bayes][5]=$p->tahun;
    $data_kawasan[$p->id_bayes][6]=$p->id_kecamatan;
    $data_kawasan[$p->id_bayes][7]=$p->nm_kecamatan;
    $data_kawasan[$p->id_bayes][8]=$p->id_kabupaten;
    $data_kawasan[$p->id_bayes][9]=$p->nm_kabupaten;

?>
<?php } ?>        
<script type="text/javascript">
  function initMap(){
    var map;
    var polygon=[]; 
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat:-3.635299, lng: 102.321318}, 
          zoom: 8
        });
       
        var data = <?php echo json_encode($data_kawasan)?>;
        var infowindow_kel=[];

        for(var x in data){
          var j = [];
          var kl = data[x][4];
          var ll = data[x][2].split("\n");


          for(var i=1;i<=ll.length;i++){
          var longlat = ll[i-1].split(",");
          j.push(new google.maps.LatLng(parseFloat(longlat[0]), parseFloat(longlat[1]))); 
      }
      
      
      var info = "<h3>"+data[x][1]+"</h3>";


      if(kl=='rendah'){
        color="green"
      }else if(kl=='sedang'){
        color="yellow"                
      }else if(kl=='tinggi'){
        color="red"               
      }

      var polygon_x = new google.maps.Polygon({
        paths: j,
          strokeColor: color,
          strokeOpacity: 0.8,
          strokeWeight: 3,
          fillColor: color,
          fillOpacity: 0.35
        });

      polygon.push(polygon_x);
      infowindow = new google.maps.InfoWindow({content: info,maxWidth:1000});
        infowindow_kel.push(infowindow);
        }

        for(var m=0; m<=polygon.length;m++){
      polygon[m].setMap(map);
      google.maps.event.addListener(polygon[m], 'click', (function(m) {
        infowindow_kel[m].close();
          return function(event){
            infowindow_kel[m].setPosition(event.latLng) 
          infowindow_kel[m].open(map);
        }
      })(m)); 
    }  
  }

  </script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkVqDh3ZeTTfLOxCm557neqs51wnzg-SM&callback=initMap">
 </script>