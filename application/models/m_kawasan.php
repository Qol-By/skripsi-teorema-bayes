<?php
defined('BASEPATH') OR exit('No direct script access allowed');
  class m_kawasan extends CI_Model {
    private $tb_desa = 'tb_desa';
    private $pk_desa = 'id_desa';

	function get_all_kawasan(){
		$hsl=$this->db->query("
			select tb_desa.id_desa, tb_kabupaten.nm_kabupaten, tb_kecamatan.id_kecamatan, tb_kecamatan.nm_kecamatan, tb_desa.nm_desa, tb_desa.polygon_desa,tb_desa.marker_desa 
			from tb_desa
			JOIN tb_kabupaten on tb_desa.id_kabupaten = tb_kabupaten.id_kabupaten 
			JOIN tb_kecamatan on tb_desa.id_kecamatan= tb_kecamatan.id_kecamatan");
	 	return $hsl;	
	}

		public function get_kab(){
			$this->db->order_by('nm_kabupaten', 'asc');
            return $this->db->get('tb_kabupaten')->result();
		}
        public function get_kec()
        {
            // kita joinkan tabel kota dengan provinsi
            $this->db->order_by('nm_kecamatan', 'asc');
            $this->db->join('tb_kabupaten', 'tb_kecamatan.id_kabupaten = tb_kabupaten.id_kabupaten');
            return $this->db->get('tb_kecamatan')->result();
        }

        public function get_des()
        {
            // kita joinkan tabel kota dengan provinsi
            $this->db->order_by('nm_desa', 'asc');
            $this->db->join('tb_kecamatan', 'tb_desa.id_kecamatan = tb_kecamatan.id_kecamatan');
            return $this->db->get('tb_desa')->result();
        }

        public function get_desa_by_id($id_desa){
            $this->db->where('id_desa', $id_desa);
            $this->db->join('tb_kecamatan', 'tb_desa.id_kecamatan = tb_kecamatan.id_kecamatan');
            $this->db->join('tb_kabupaten', 'tb_kecamatan.id_kabupaten = tb_kabupaten.id_kabupaten');
            return $this->db->get('tb_desa')->row();
		}

		public function get_kecamatan_by_id($id_kecamatan){
            $this->db->where('id_kecamatan', $id_kecamatan);
            $this->db->join('tb_kabupaten', 'tb_kecamatan.id_kabupaten = tb_kabupaten.id_kabupaten');
            return $this->db->get('tb_kecamatan')->row();
		}

//--------kebaupaten--------//
	function get_all_kabupaten(){
		$hsl=$this->db->query("Select * from tb_kabupaten order by nm_kabupaten asc");
		return $hsl;
	}

	function simpan_kabupaten($nm_kabupaten){
		$hsl=$this->db->query("insert into tb_kabupaten(nm_kabupaten) values ('$nm_kabupaten')");
		return $hsl;
	}
	function hapus_kabupatan($id_kabupaten){
		$hsl=$this->db->query("delete from tb_kabupaten where id_kabupaten='$id_kabupaten'");
		return $hsl;
	}
	function ubah_kabupaten($id_kabupaten,$nm_kabupaten){

		$hsl=$this->db->query("update tb_kabupaten set nm_kabupaten='$nm_kabupaten' where id_kabupaten ='$id_kabupaten'");
		return $hsl;
	}

//--------kecamatan--------//
	function get_all_kecamatan(){
		$hsl=$this->db->query("select tb_kecamatan.id_kecamatan, tb_kecamatan.id_kabupaten,tb_kabupaten.nm_kabupaten, tb_kecamatan.nm_kecamatan
			from tb_kecamatan
			JOIN tb_kabupaten on tb_kecamatan.id_kabupaten = tb_kabupaten.id_kabupaten");
		return $hsl;
	}

	function get_kecamatan(){
		$hsl=$this->db->query("Select * from tb_kecamatan");
		return $hsl;
	}
	function get_kecamatan2($id_kecamatanz){
		$hsl=$this->db->query("Select * from tb_kecamatan where id_kecamatan='$id_kecamatanz'");
		return $hsl;
	}		


	function simpan_kecamatan($id_kabupaten,$nm_kabupaten){
		$hsl=$this->db->query("insert into tb_kecamatan(id_kabupaten,nm_kecamatan) values ('$id_kabupaten','$nm_kabupaten')");
		return $hsl;
	}
	function hapus_kecamatan($id_kecamatan){
		$hsl=$this->db->query("delete from tb_kecamatan where id_kecamatan='$id_kecamatan'");
		return $hsl;
	}
	function ubah_kecamatan($id_kecamatan,$id_kabupatenx,$nm_kecamatan){

		$hsl=$this->db->query("update tb_kecamatan set id_kabupaten ='$id_kabupatenx',nm_kecamatan='$nm_kecamatan' where id_kecamatan='$id_kecamatan'");
		return $hsl;
	}


//--------desa--------//
	function get_all_desa(){
		$hsl=$this->db->query("Select * from tb_desa");
		return $hsl;
	}
	function get_polygon(){
		$hsl=$this->db->query("Select * from tb_desa where polygon_desa");
		return $hsl;
	}
	function get_marker(){
		$hsl=$this->db->query("Select * from tb_desa where marker_desa");
		return $hsl;
	}

	function simpan_desa($id_kabupaten,$id_kecamatan,$nm_desa,$polygon,$marker){
		$hsl=$this->db->query("insert into tb_desa(id_kabupaten,id_kecamatan,nm_desa,polygon_desa,marker_desa) values ('$id_kabupaten','$id_kecamatan','$nm_desa','$polygon','$marker')");
		return $hsl;
	}

	function hapus_desa($id_desa){
		$hsl=$this->db->query("delete from tb_desa where id_desa='$id_desa'");
		return $hsl;
	}
	 public function getById($id_desa) {
        $this->db->where($this->pk_desa,$id_desa);
        return $this->db->get($this->tb_desa);
    }

    public function ubah_desa($id,$data) {
        $this->db->where($this->pk_desa, $id);
        $this->db->update($this->tb_desa, $data);
    }

	function update_desa($id_desa,$id_kabupatenx,$id_kecamatanx,$nm_desa,$polygon,$marker){
	
		$hsl=$this->db->query("update tb_desa set id_kabupaten='$id_kabupatenx',id_kecamatan='$id_kecamatanx',nm_desa='$nm_desa',polygon_desa='$polygon',marker_desa='$marker' where id_desa ='$id_desa'");
		return $hsl;
	}	

}