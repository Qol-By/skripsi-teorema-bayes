<?php 
$this->load->view('backend/v_header');
?>
  <aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
        <li class="header">Menu Utama</li>
        <li>
          <a href="<?php echo base_url().'index.php/backend/beranda'?>">
            <i class="fa fa-home"></i> <span>Beranda</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <li class="treeview">
          <a>
            <i class="fa fa-archive"></i>
            <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="<?php echo base_url().'index.php/backend/kawasan'?>"> <i class="fa fa-list-alt"></i>Kawasan</li>
            <li><a href="<?php echo base_url().'index.php/backend/parameter'?>"><i class="fa fa-list"></i>Parameter</a></li>
            <li><a href="<?php echo base_url().'index.php/backend/klasifikasi'?>"><i class="fa fa-wrench"></i>Klasifikasi Laju Abrasi Pantai</a></li>
          </ul>
        </li>
         <li><a href="<?php echo base_url().'index.php/backend/pemetaan'?>"> <i class="fa fa-map-o"></i> <span>Peta Kawasan Abrasi</span><span class="pull-right-container"><small class="label pull-right"></small></span></a></li>
         <li>
          <a data-target="#Modalkeluar" data-toggle="modal">
            <i class="fa fa-sign-out"></i> <span>Keluar</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Ubah Data Klasifiaksi
        <small></small>
      </h1>
    </section>
		

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
              <h4 class="modal-title" id="myModalLabel">Data Kawasan</h4>
            </div>

            <form class="form-horizontal" action="<?php echo base_url().'index.php/backend/kawasan/update_desa'?>" method="post" enctype="multipart/form-data">
              <div class="modal-body">
                   
              <div class="form-group">
                  <label class="col-sm-4 control-label">Kabupaten</label> 

                  <div class="col-sm-7">
                  <input value='<?php echo $id_desa;?>' type="hidden" name="xid_desa"> 
                        <select class="form-control" name="xid_kabupaten" id="xid_kabupaten">
                            <option value="">Please Select</option>
                            <?php
                            foreach ($kabupaten as $prov) {
                                ?>
                                <option <?php echo $kabupaten_selected == $prov->id_kabupaten ? 'selected="selected"' : '' ?> 
                                    value="<?php echo $prov->id_kabupaten ?>"><?php echo $prov->nm_kabupaten ?></option>
                                <?php
                            }
                            ?>
                        </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-4 control-label">Kecamatan </label>
                  <div class="col-sm-7">
                        <select class="form-control" name="xid_kecamatan" id="xid_kecamatan">
                            <option value="">Please Select</option>
                            <?php
                            foreach ($kecamatan as $kec) {
                                ?>
                                <option <?php echo $kecamatan_selected == $kec->id_kecamatan ? 'selected="selected"' : '' ?> 
                                     class="<?php echo $kec->id_kabupaten ?>" value="<?php echo $kec->id_kecamatan ?>"><?php echo $kec->nm_kecamatan ?></option>
                                <?php
                            }
                            ?>
                        </select>
                  </div>
                </div>


                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Nama Desa</label>
                  <div class="col-sm-7">
                    <input type="text" name="xnm_desa" class="form-control" id="inputUserName" placeholder="Nama Desa" value="<?php echo $nm_desa;?>" required >
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Polygon Desa</label>
                  <div class="col-sm-7">
                       <textarea type="textarea" name="xpolygon_desa" class="form-control"required><?php echo $polygon;?></textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">marker Desa</label>
                  <div class="col-sm-7">
                    <input type="text" name="xmarker_desa" class="form-control" id="inputUserName" value="<?php echo $marker;?>" placeholder="Nama Desa" required>
                  </div>
                </div>   
                            
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
              </div>
            </form>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2019.</strong> All rights reserved.
  </footer>
  
  <?php 
    $this->load->view('backend/v_footer');
  ?>
