    <?php
      foreach ($klasifikasi->result() as $klas) :
    ?> 
      <div class="modal fade" id="hapusklasifikasi<?php echo $klas->id_bayes;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel">Data Klasififkasi</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url().'index.php/backend/klasifikasi/hapus_klasifikasi'?>" method="post" enctype="multipart/form-data">
              <div class="modal-body">
                <div class="form-group">
                  <div class="col-sm-7">
                     <input value='<?php echo $klas->id_bayes;?>' type="hidden" name="xid_bayes"> 
                        <p>Apakah Anda yakin mau menghapus Data Desa <b><?php echo $klas->nm_desa;?></b> Kecamatan <b><?php echo $klas->nm_kecamatan;?></b> Kabupaten <b><?php echo $klas->nm_kabupaten;?></b> ?</p>
                  </div>
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger btn-flat" id="simpan">Hapus</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <?php endforeach;?>


      <div class="modal fade" id="exceltahun" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel">Tahun Klasififkasi</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url().'index.php/backend/klasifikasi/excel'?>" method="post" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Tahun</label>
                  <div class="col-sm-7">
                    <select name="tahun" class="form-control select2" required>
                      <option value="">-Pilih-</option>
                      <option value="2016">2016</option>
                      <option value="2017">2017</option>
                      <option value="2018">2018</option>
                        
                    </select>
                  </div>
                </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Export</button>
              </div>
            </form>
          </div>
        </div>
      </div>
