<?php
class kawasan extends CI_Controller {
	function __construct() 
	{
		parent::__construct();
//            jika belum login redirect ke login
		if ($this->session->userdata('logged')<>1) {
			redirect(site_url('login'));
		}
		$this->load->model('m_kawasan');
	}

	public function index()
	{

		$data['kawasan']=$this->m_kawasan->get_all_kawasan();	
		$data['kabupaten']=$this->m_kawasan->get_all_kabupaten();
		$data['kecamatan']=$this->m_kawasan->get_all_kecamatan();
		$data['desa']=$this->m_kawasan->get_all_desa();

		$data['dtkecamatan']=$this->m_kawasan->get_kec();
		$data['kabupaten_selected']= '';
        $data['kecamatan_selected' ]= '';


		$this->load->view('backend/v_kawasan',$data);
	}

        public function ubah($id_desa) {

        	$selected = $this->m_kawasan->get_desa_by_id($id_desa);
            $row = $this->m_kawasan->getById($id_desa)->row();
            $data = array(
	            'kabupaten' => $this->m_kawasan->get_kab(),
	            'kecamatan' => $this->m_kawasan->get_kec(),            	
            	'kabupaten_selected' => $selected->id_kabupaten,
            	'kecamatan_selected' => $selected->id_kecamatan,
            	
            	'id_desa'=> $row->id_desa,
            	'id_kabupaten'=> $row->id_kabupaten,
            	'id_kecamatan'=> $row->id_kecamatan,
            	'nm_desa'=> $row->nm_desa,
            	'polygon'=> $row->polygon_desa,
            	'marker'=> $row->marker_desa,
            	
            );

            $this->load->view('backend/v_ubah_kawasan',$data);
        }

	function update_desa(){
		$id_desa=strip_tags($this->input->post('xid_desa'));
		$id_kabupatenx=strip_tags($this->input->post('xid_kabupaten'));		
		$id_kecamatanx=strip_tags($this->input->post('xid_kecamatan'));
		$nm_desa=strip_tags($this->input->post('xnm_desa'));
		$polygon=strip_tags($this->input->post('xpolygon_desa'));
		$marker=strip_tags($this->input->post('xmarker_desa'));

		$this->m_kawasan->update_desa($id_desa,$id_kabupatenx,$id_kecamatanx,$nm_desa,$polygon,$marker);
		echo $this->session->set_flashdata('msg','success');
		redirect('backend/kawasan');
	}




//--------kabupaten--------//
	function simpan_kabupaten(){
		$nm_kabupaten=strip_tags($this->input->post('xnm_kabupaten'));
		$this->m_kawasan->simpan_kabupaten($nm_kabupaten);
		echo $this->session->set_flashdata('msg','success');
		redirect('backend/kawasan');
	}

	function ubah_kabupaten(){
		$id_kabupaten=strip_tags($this->input->post('xid_kabupaten'));
		$nm_kabupaten=strip_tags($this->input->post('xnm_kabupaten'));
		$this->m_kawasan->ubah_kabupaten($id_kabupaten,$nm_kabupaten);
		echo $this->session->set_flashdata('msg','success');
		redirect('backend/kawasan');
	}

	function hapus_kabupaten(){
		$id_kabupaten=strip_tags($this->input->post('xid_kabupaten'));
		$this->m_kawasan->hapus_kabupatan($id_kabupaten);
		echo $this->session->set_flashdata('msg','hapusdata');
		redirect('backend/kawasan');
	}	
//--------kecamatan--------//
	function simpan_kecamatan(){
		$id_kabupaten=strip_tags($this->input->post('xid_kabupaten'));
		$nm_kecamatan=strip_tags($this->input->post('xnm_kecamatan'));
		$this->m_kawasan->simpan_kecamatan($id_kabupaten,$nm_kecamatan);
		echo $this->session->set_flashdata('msg','success');
		redirect('backend/kawasan');
	}
	
	function ubah_kecamatan(){
		$id_kecamatan=strip_tags($this->input->post('xid_kecamatan'));
		$id_kabupatenx=strip_tags($this->input->post('xid_kabupaten'));
		$nm_kecamatan=strip_tags($this->input->post('xnm_kecamatan'));
		$this->m_kawasan->ubah_kecamatan($id_kecamatan,$id_kabupatenx,$nm_kecamatan);
		echo $this->session->set_flashdata('msg','success');
		redirect('backend/kawasan');
	}

	function hapus_kecamatan(){
		$id_kecamatan=strip_tags($this->input->post('xid_kecamatan'));
		$this->m_kawasan->hapus_kecamatan($id_kecamatan);
		echo $this->session->set_flashdata('msg','hapusdata');
		redirect('backend/kawasan');
	}
//--------desa--------//	
	function simpan_desa(){
    

		$id_kabupaten=strip_tags($this->input->post('xid_kabupaten'));	
 		$id_kecamatan=strip_tags($this->input->post('xid_kecamatan')); 
		$nm_desa=strip_tags($this->input->post('xnm_desa'));
		$polygon=strip_tags($this->input->post('xpolygon_desa'));
		$marker=strip_tags($this->input->post('xmarker_desa'));		
		$this->m_kawasan->simpan_desa($id_kabupaten,$id_kecamatan,$nm_desa,$polygon,$marker);
		echo $this->session->set_flashdata('msg','success');
		redirect('backend/kawasan');                                                                                                            
	}



	function hapus_desa(){
		$id_desa=strip_tags($this->input->post('xid_desa'));
		$this->m_kawasan->hapus_desa($id_desa);
		echo $this->session->set_flashdata('msg','success');
		redirect('backend/kawasan');
	}
}
